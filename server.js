//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var bodyparser = require('body-parser')
  app.use(bodyparser.json())
  app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })
var movimientosv2JSON = require('./movimientosv2.json');
var requestjson = require('request-json');
var urlClientesMLab = "https://api.mlab.com/api/1/databases/kgarrido/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientesMLab);
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/kgarrido/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res) {
  //res.send('Hemos recibido su petición get');
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/',function(req,res) {
  res.send('Hemos recibido su petición post')
})

app.put('/',function(req,res) {
  res.send('Hemos recibido su petición put')
})

app.delete('/',function(req,res) {
  res.send('Hemos recibido su petición detele')
})

app.get('/v1/Clientes/:idcliente',function(req,res){
  res.send('Aquí tiene al cliente número: ' + req.params.idcliente);
})

app.get('/v1/movimientos',function(req,res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/movimientos',function(req,res){
  res.json(movimientosv2JSON);
})

app.get('/v2/movimientos/:index',function(req,res){
  console.log(req.params.index);
  res.send(movimientosv2JSON[req.params.index - 1]);
})

app.get('/v2/movimientosquery',function(req,res){
  console.log(req.query);
  res.send('recibido');
})

app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body
  nuevo.id = movimientosv2JSON.length + 1
  movimientosv2JSON.push(nuevo)
  res.send('ḿovimiento dado de alta')
})

app.put('/v2/movimientos',function(req,res){
  res.send('Hemos recibido su peticion put de actualizacion de movimientos');
})

app.get('/v3/Clientes',function(req,res){
  clienteMLab.get('', function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/v3/Clientes',function(req,res){
  clienteMLab.post('',req.body,function(err, resM, body){
    res.send(body);
  })
})

app.post('/v4/login',function(req,res){
  var email = req.body.email
  var password = req.body.password
  //var emailh = req.headers.email
  //var passwordh = req.headers.password

  //console.log("Headers: "+emailh+" "+passwordh);

  var query = 'q={"email":"'+email+'","password":"'+password+'"}';
  clienteMLabRaiz = requestjson.createClient(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query);
  console.log(urlMlabRaiz + "/Usuarios?" + apiKey + "&" + query);

  clienteMLabRaiz.get('',function(err, resM, body){
    if (!err){
      if (body.length == 1){ //login ok
        res.status(200).send('Usuario logado');
      } else {
        res.status(404).send('El usuario no existe, Registrese');
      }
    } else {
      console.log(body);
    }
  })
})
